﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Custo.Backend.Restaurant.DataAccess.SqlHelper
{
    public class SqlMapper
    {
        public void Map<T>(T source, SqlParameterCollection dest)
        {
            foreach (var prop in source.GetType().GetTypeInfo().GetProperties())
            {
                object val = prop.GetValue(source, null);
                if (prop.PropertyType == typeof(DateTime) && ((DateTime)val).Year <= 1900)
                {
                    val = DBNull.Value;
                }
                dest.Add(new SqlParameter(prop.Name, val ?? DBNull.Value));
            }
        }
        public List<SqlParameter> Map(object source, bool SkipMappingPrimaryKey = true)
        {
            var dest = new List<SqlParameter>();
            var primaryId = source.GetType().Name + "Id";
            foreach (var prop in source.GetType().GetTypeInfo().GetProperties())
            {
                object val = prop.GetValue(source, null);
                if (prop.PropertyType == typeof(DateTime) && ((DateTime)val).Year <= 1900)
                {
                    val = DBNull.Value;
                }
                if (SkipMappingPrimaryKey && prop.Name.ToLower() != primaryId.ToLower())
                    dest.Add(new SqlParameter(prop.Name, val ?? DBNull.Value));
                else
                    dest.Add(new SqlParameter(prop.Name, val ?? DBNull.Value));
            }
            return dest;
        }
        public T Map<T>(SqlDataReader reader)
        {
            if (typeof(T).GetTypeInfo().IsValueType || typeof(T) == typeof(string))
            {
                var value = reader[0];
                return (value.GetType() != typeof(DBNull)) ? (T)Convert.ChangeType(value, typeof(T)) : default(T);
            }
            else
            {
                var element = Activator.CreateInstance<T>();
                var columnList = reader.GetColumnSchema().Select(x => x.ColumnName);
                var properties = element.GetType().GetTypeInfo().GetProperties();
                foreach (var prop in properties)
                {
                    if (!columnList.Contains(prop.Name))
                        continue;

                    var o = reader[prop.Name];

                    if (o.GetType() != typeof(DBNull)) prop.SetValue(element, ChangeType(o, prop.PropertyType), null);
                }
                return element;
            }
        }
        private object ChangeType(object value, Type conversion)
        {
            var t = conversion;

            if (t.GetTypeInfo().IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }

                t = Nullable.GetUnderlyingType(t); ;
            }
            if (t.GetTypeInfo().IsEnum)
                return Enum.Parse(t, value?.ToString());

            return Convert.ChangeType(value, t);
        }
    }
}