﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Custo.Backend.Restaurant.DataAccess.SqlHelper
{
    public interface ISql
    {
        int ExecuteNonQuery(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, object objParams, int cmdTimeout = 300, bool skipMappingPrimaryKey = true);
        int ExecuteNonQuery(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 300);
        int ExecuteNonQuery(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 300, bool skipMappingPrimaryKey = true);
        int ExecuteNonQuery(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 300);
        T GetEntity<T>(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, object objParams, int cmdTimeout = 300, bool skipMappingPrimaryKey = true);
        T GetEntity<T>(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 300);
        T GetEntity<T>(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 300, bool skipMappingPrimaryKey = true);
        T GetEntity<T>(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 300);
        List<T> GetEntityList<T>(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 300, bool skipMappingPrimaryKey = true);
        List<T> GetEntityList<T>(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 300);
        DataSet GetDataSet(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 300);
        DataSet GetDataSet(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 300, bool skipMappingPrimaryKey = true);
        SqlConnection GetSqlConnection();
    }
}
