﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Custo.Backend.Restaurant.DataAccess.SqlHelper
{
    public enum SqlCommandType
    {
        Text = CommandType.Text,
        StoredProcedure = CommandType.StoredProcedure,
        TableDirect = CommandType.TableDirect
    }
    public class Sql : SqlMapper, ISql
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionstring">SQL Connection String</param>
        public Sql(string connectionstring)
        {
            SqlConnectionString = connectionstring;
        }

        public string SqlConnectionString { get; set; }

        /// <summary>
        /// Instantiate a SqlConnection object 
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetSqlConnection()
        {
            return new SqlConnection(SqlConnectionString);
        }

        private void ValidateRequest(string cmdtext)
        {
            if (string.IsNullOrEmpty(cmdtext))
            {
                throw new ArgumentException("Command text cannot be empty");
            }
        }

        private void ValidateRequest(SqlConnection conn, SqlTransaction trnx, string cmdtext)
        {
            if (string.IsNullOrEmpty(cmdtext))
            {
                throw new ArgumentException("Command text cannot be empty");
            }

            if (conn == null)
            {
                throw new ArgumentException("SqlConnection cannot be null");
            }
            else if (conn.State == ConnectionState.Closed)
            {
                throw new ArgumentException("SqlConnection not is in Closed state");
            }

            if (trnx == null)
            {
                throw new ArgumentException("SqlTransaction cannot be null");
            }
        }
        /// <summary>
        /// Gets the entity of given type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="cmdParams">Command parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public T GetEntity<T>(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 60)
        {
            ValidateRequest(cmdtext);
            T result = default(T);
            using (var sqlConnection = GetSqlConnection())
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(cmdtext, sqlConnection))
                {
                    sqlCommand.CommandType = cmdType;
                    sqlCommand.CommandTimeout = cmdTimeout;

                    if (cmdParams?.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(cmdParams);
                    }

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result = Map<T>(reader);
                            break;
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the entity of given type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="objParams">Entity that holds the parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public T GetEntity<T>(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 60, bool skipMappingPrimaryKey = true)
        {
            ValidateRequest(cmdtext);
            SqlParameter[] cmdParams = null;
            if (objParams != null)
            {
                cmdParams = Map(objParams, skipMappingPrimaryKey)?.ToArray();
            }
            return GetEntity<T>(cmdtext, (CommandType)cmdType, cmdParams, cmdTimeout);
        }

        /// <summary>
        /// Gets the entity of given type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conn">Sql Connection object</param>
        /// <param name="trnx">Transaction under which the command executes</param>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="cmdParams">Command parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public T GetEntity<T>(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 60)
        {
            ValidateRequest(conn, trnx, cmdtext);
            T result = default(T);
            using (var sqlCommand = new SqlCommand(cmdtext, conn, trnx))
            {
                sqlCommand.CommandType = cmdType;
                sqlCommand.CommandTimeout = cmdTimeout;

                if (cmdParams?.Length > 0)
                {
                    sqlCommand.Parameters.AddRange(cmdParams);
                }

                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result = Map<T>(reader);
                        break;
                    }
                }

            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conn">Sql Connection object</param>
        /// <param name="trnx">Transaction under which the command executes</param>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="objParams">Entity that holds the parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public T GetEntity<T>(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, object objParams, int cmdTimeout = 60, bool skipMappingPrimaryKey = true)
        {
            ValidateRequest(conn, trnx, cmdtext);
            SqlParameter[] cmdParams = null;
            if (objParams != null)
            {
                cmdParams = Map(objParams, skipMappingPrimaryKey)?.ToArray();
            }
            return GetEntity<T>(conn, trnx, cmdtext, cmdType, cmdParams, cmdTimeout);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="cmdParams">Command parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public List<T> GetEntityList<T>(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 60)
        {
            ValidateRequest(cmdtext);
            List<T> result = new List<T>();
            using (var sqlConnection = GetSqlConnection())
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(cmdtext, sqlConnection))
                {
                    sqlCommand.CommandType = cmdType;
                    sqlCommand.CommandTimeout = cmdTimeout;

                    if (cmdParams?.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(cmdParams);
                    }

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(Map<T>(reader));
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="objParams">Entity that holds the parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public List<T> GetEntityList<T>(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 60, bool skipMappingPrimaryKey = true)
        {
            ValidateRequest(cmdtext);
            SqlParameter[] cmdParams = null;
            if (objParams != null)
            {
                cmdParams = Map(objParams, skipMappingPrimaryKey)?.ToArray();
            }
            return GetEntityList<T>(cmdtext, (CommandType)cmdType, cmdParams, cmdTimeout);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="cmdParams">Command parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public int ExecuteNonQuery(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 60)
        {
            ValidateRequest(cmdtext);
            int result;
            try
            {
                using (var sqlConnection = GetSqlConnection())
                {
                    sqlConnection.Open();

                    using (var sqlCommand = new SqlCommand(cmdtext, sqlConnection))
                    {
                        sqlCommand.CommandType = cmdType;
                        sqlCommand.CommandTimeout = cmdTimeout;

                        if (cmdParams?.Length > 0)
                        {
                            sqlCommand.Parameters.AddRange(cmdParams);
                        }

                        result = sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="objParams">Entity that holds the parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public int ExecuteNonQuery(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 60, bool skipMappingPrimaryKey = true)
        {
            ValidateRequest(cmdtext);
            SqlParameter[] cmdParams = null;
            if (objParams != null)
            {
                cmdParams = Map(objParams, skipMappingPrimaryKey)?.ToArray();
            }
            return ExecuteNonQuery(cmdtext, (CommandType)cmdType, cmdParams, cmdTimeout);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn">Sql Connection object</param>
        /// <param name="trnx">Transaction under which the command executes</param>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="cmdParams">Command parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public int ExecuteNonQuery(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 60)
        {
            ValidateRequest(conn, trnx, cmdtext);
            int result;
            try
            {
                using (var sqlCommand = new SqlCommand(cmdtext, conn, trnx))
                {
                    sqlCommand.CommandType = cmdType;
                    sqlCommand.CommandTimeout = cmdTimeout;

                    if (cmdParams?.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(cmdParams);
                    }

                    result = sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn">Sql Connection object</param>
        /// <param name="trnx">Transaction under which the command executes</param>
        /// <param name="cmdtext">Sql Command Text</param>
        /// <param name="cmdType">Sql Command Type</param>
        /// <param name="objParams">Entity that holds the parameters needed for the query</param>
        /// <param name="cmdTimeout">Command timeout default to 60 sec.</param>
        /// <returns></returns>
        public int ExecuteNonQuery(SqlConnection conn, SqlTransaction trnx, string cmdtext, CommandType cmdType, object objParams, int cmdTimeout = 60, bool skipMappingPrimaryKey = true)
        {
            ValidateRequest(conn, trnx, cmdtext);
            SqlParameter[] cmdParams = null;
            if (objParams != null)
            {
                cmdParams = Map(objParams, skipMappingPrimaryKey)?.ToArray();
            }
            return ExecuteNonQuery(conn, trnx, cmdtext, cmdType, cmdParams, cmdTimeout);
        }

        public DataSet GetDataSet(string cmdtext, CommandType cmdType, SqlParameter[] cmdParams, int cmdTimeout = 60)
        {
            DataSet dataSet = new DataSet();
            ValidateRequest(cmdtext);
            using (var sqlConnection = GetSqlConnection())
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(cmdtext, sqlConnection))
                {
                    sqlCommand.CommandType = cmdType;
                    sqlCommand.CommandTimeout = cmdTimeout;

                    if (cmdParams?.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(cmdParams);
                    }

                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                    {
                        sqlDataAdapter.Fill(dataSet);
                    }
                }
            }
            return dataSet;
        }

        public DataSet GetDataSet(string cmdtext, SqlCommandType cmdType, object objParams, int cmdTimeout = 60, bool skipMappingPrimaryKey = true)
        {
            ValidateRequest(cmdtext);
            SqlParameter[] cmdParams = null;
            if (objParams != null)
            {
                cmdParams = Map(objParams, skipMappingPrimaryKey)?.ToArray();
            }
            return GetDataSet(cmdtext, (CommandType)cmdType, cmdParams, cmdTimeout);
        }
    }
}

