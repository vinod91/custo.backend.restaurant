﻿using System.Collections.Generic;
using DE = Custo.Backend.Restaurant.Entities.DB;
using Custo.Backend.Restaurant.DataAccess.SqlHelper;
using Amazon.Runtime;
using Amazon.DynamoDBv2;
using Amazon;
using Amazon.DynamoDBv2.Model;
using Amazon.DynamoDBv2.DocumentModel;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Custo.Backend.Restaurant.Entities.DB;
using System.Linq;
using Newtonsoft.Json;

namespace Custo.Backend.Restaurant.DataAccess
{
    public class RestaurantRepository : IRestaurantRepository
    {
        private readonly ISql _sql;
        AmazonDynamoDBClient _client;
        public RestaurantRepository()
        {
            _sql = _sql = new Sql("Data Source=custo.chk5kr5tvbe7.us-east-1.rds.amazonaws.com,1433;user id=custouser; password=Custo123; initial catalog = Custo;");
            var credentials = new BasicAWSCredentials("AKIAJFK74RHXJMVFEKVQ", "F9hVdrtds5fByi72NgjGKVYbNHT7SeqqMML8dGt7");
            _client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USEast1);
        }
        public List<DE.Restaurant> GetResturants(float latitude, float longitude)
        {
            var restaurants = _sql.GetEntityList<DE.Restaurant>(DBConstants.GetRestaurantsBasedOnLocation, SqlCommandType.StoredProcedure, new { Latitude = latitude, Longitude = longitude });
            return restaurants;
        }

        public List<DE.Restaurant> GetResturants(string searchParams)
        {
            var restaurants = _sql.GetEntityList<DE.Restaurant>(DBConstants.GetRestaurantsBasedOnNameOrFood, SqlCommandType.StoredProcedure, new { SearchParams = searchParams });
            return restaurants;
        }

        public async Task<RestaurantDetails> FetchRestaurantDetails(int restaurantId)
        {
            
            Table table = Table.LoadTable(_client, "RestaurantDetails");
            GetItemOperationConfig getItemOperationConfig = new GetItemOperationConfig();
            var restaurantDynamoDB = await table.GetItemAsync(restaurantId, getItemOperationConfig);
            var restaurantDetails = JsonConvert.DeserializeObject<RestaurantDetails>(restaurantDynamoDB.ToJson());
            return restaurantDetails;
            //QueryFilter filter = new QueryFilter();
            //filter.AddCondition("RestaurantId", QueryOperator.Equal, restaurantId); 

            //QueryOperationConfig config = new QueryOperationConfig()
            //{
            //    Filter = filter,
            //    Select = SelectValues.AllAttributes,
            //    ConsistentRead = true
            //};
            //var tableResult = table.Query(config);

            //DynamoDBContext context = new DynamoDBContext(_client);

            //List<ScanCondition> conditions = new List<ScanCondition>();
            //conditions.Add(new ScanCondition("RestaurantId", ScanOperator.Equal, restaurantId));
            //var allDocs =  context.ScanAsync<dynamic>(conditions).GetRemainingAsync().Result;
            //var savedState = allDocs.FirstOrDefault();

            ////get by id
            //var record = await context.LoadAsync<dynamic>(restaurantId);

            //var listTablesResponse = await _client.ListTablesAsync();
        }
    }
}
