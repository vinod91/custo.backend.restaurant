﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Custo.Backend.Restaurant.DataAccess
{
    public class DBConstants
    {
        #region Stored Procedure Names
        //Restaurant
        public static string GetRestaurantsBasedOnLocation = "GetResturantsBasedOnCoordinate";
        public static string GetRestaurantsBasedOnNameOrFood = "GetResturantsBasedOnNameOrFood";
        #endregion
    }
}
