﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Custo.Backend.Restaurant.Entities.DB;
using DE = Custo.Backend.Restaurant.Entities.DB;

namespace Custo.Backend.Restaurant.DataAccess
{
    public interface IRestaurantRepository
    {
        List<DE.Restaurant> GetResturants(float latitude, float longitude);
        List<DE.Restaurant> GetResturants(string searchParam);
        Task<RestaurantDetails> FetchRestaurantDetails(int restaurantId);
    }
}
