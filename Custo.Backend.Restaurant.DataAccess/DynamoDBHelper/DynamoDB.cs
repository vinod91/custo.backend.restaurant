﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

namespace Custo.Backend.Restaurant.DataAccess.DynamoDBHelper
{
    public class DynamoDB : IDynamoDB
    {
        private readonly AmazonDynamoDBClient _client;
        private readonly DynamoDBContextConfig _ContextConfig;

        public DynamoDB(string tablePrefix)
        {
            _client = new AmazonDynamoDBClient(new AmazonDynamoDBConfig { RegionEndpoint = RegionEndpoint.USEast1 });

            _ContextConfig = new DynamoDBContextConfig { TableNamePrefix = tablePrefix };

        }
        public DynamoDB(RegionEndpoint regionEndpoint, string awsKey = null, string awsSecret = null)
        {
            if (awsKey != null && awsSecret != null)
                _client = new AmazonDynamoDBClient(awsKey, awsSecret, new AmazonDynamoDBConfig { RegionEndpoint = regionEndpoint });
            else
                _client = new AmazonDynamoDBClient(new AmazonDynamoDBConfig { RegionEndpoint = regionEndpoint });
        }

        public async Task<List<string>> GetTablesList()
        {
            var listTablesResponse = await _client.ListTablesAsync();
            return listTablesResponse.TableNames;
        }

        public async Task<List<string>> GetTableDetails(string table)
        {
            List<string> tableDetails = new List<string>();

            DescribeTableRequest describeTableRequest = new DescribeTableRequest(table);
            DescribeTableResponse describeTableResponse = await _client.DescribeTableAsync(describeTableRequest);
            TableDescription tableDescription = describeTableResponse.Table;

            tableDetails.Add(string.Format("Printing information about table {0}:", tableDescription.TableName));
            tableDetails.Add(string.Format("Created at: {0}", tableDescription.CreationDateTime));

            List<KeySchemaElement> keySchemaElements = tableDescription.KeySchema;
            foreach (KeySchemaElement schema in keySchemaElements)
            {
                tableDetails.Add(string.Format("Key name: {0}, key type: {1}", schema.AttributeName, schema.KeyType));
            }
            tableDetails.Add(string.Format("Item count: {0}", tableDescription.ItemCount));

            ProvisionedThroughputDescription throughput = tableDescription.ProvisionedThroughput;
            tableDetails.Add(string.Format("Read capacity: {0}", throughput.ReadCapacityUnits));
            tableDetails.Add(string.Format("Write capacity: {0}", throughput.WriteCapacityUnits));

            List<AttributeDefinition> tableAttributes = tableDescription.AttributeDefinitions;
            foreach (AttributeDefinition attDefinition in tableAttributes)
            {
                tableDetails.Add(string.Format("Table attribute name: {0}", attDefinition.AttributeName));
                tableDetails.Add(string.Format("Table attribute type: {0}", attDefinition.AttributeType));
            }
            tableDetails.Add(string.Format("Table size: {0}b", tableDescription.TableSizeBytes));
            tableDetails.Add(string.Format("Table status: {0}", tableDescription.TableStatus));
            tableDetails.Add("====================================================");

            return tableDetails;
        }

        public async Task<List<string>> GetTablesDetails()
        {
            List<string> tablesDetails = new List<string>();
            List<string> tables = await GetTablesList();

            foreach (string table in tables)
            {
                List<string> temp = await GetTableDetails(table);
                tablesDetails.AddRange(temp);
            }
            return tablesDetails;
        }

        public async Task<List<string>> CreateTableAsync(string tableName, string primaryKey, string secondaryKey)
        {
            List<string> tableCreationSteps = new List<string>();
            try
            {
                CreateTableRequest createTableRequest = new CreateTableRequest();
                createTableRequest.TableName = tableName;
                createTableRequest.ProvisionedThroughput = new ProvisionedThroughput() { ReadCapacityUnits = 1, WriteCapacityUnits = 1 };
                createTableRequest.KeySchema = new List<KeySchemaElement>()
                    {
                        new KeySchemaElement()
                        {
                            AttributeName = primaryKey,
                            KeyType = KeyType.HASH
                        },
                        new KeySchemaElement()
                        {
                            AttributeName = secondaryKey,
                            KeyType = KeyType.RANGE
                        }
                    };
                createTableRequest.AttributeDefinitions = new List<AttributeDefinition>()
                    {
                        new AttributeDefinition(){AttributeName = primaryKey, AttributeType = ScalarAttributeType.S}
                        , new AttributeDefinition(){AttributeName = secondaryKey, AttributeType = ScalarAttributeType.N}
                    };
                CreateTableResponse createTableResponse = await _client.CreateTableAsync(createTableRequest);

                TableDescription tableDescription = createTableResponse.TableDescription;
                tableCreationSteps.Add(string.Format("Table {0} creation command sent to Amazon. Current table status: {1}", tableName, tableDescription.TableStatus));

                string tableStatus = tableDescription.TableStatus.Value.ToLower();
                while (tableStatus != "active")
                {
                    tableCreationSteps.Add(string.Format("Table {0} not yet active, waiting...", tableName));
                    Thread.Sleep(2000);
                    DescribeTableRequest describeTableRequest = new DescribeTableRequest(tableName);
                    DescribeTableResponse describeTableResponse = await _client.DescribeTableAsync(describeTableRequest);
                    tableDescription = describeTableResponse.Table;
                    tableStatus = tableDescription.TableStatus.Value.ToLower();
                    tableCreationSteps.Add(string.Format("Latest status of table {0}: {1}", tableName, tableStatus));
                }

                tableCreationSteps.Add(string.Format("Table creation loop exited for table {0}, final status: {1}", tableName, tableStatus));
            }
            catch (AmazonDynamoDBException exception)
            {
                tableCreationSteps.Add(string.Concat("Exception while creating new DynamoDb table: {0}", exception.Message));
                tableCreationSteps.Add(string.Concat("Error code: {0}, error type: {1}", exception.ErrorCode, exception.ErrorType));
            }
            return tableCreationSteps;
        }

        public async Task<List<string>> DeleteTableAsync(string tableName)
        {
            List<string> tableDeletionSteps = new List<string>();
            try
            {
                DeleteTableRequest deleteTableRequest = new DeleteTableRequest(tableName);
                DeleteTableResponse deleteTableResponse = await _client.DeleteTableAsync(deleteTableRequest);
                TableDescription tableDescription = deleteTableResponse.TableDescription;
                TableStatus tableStatus = tableDescription.TableStatus;
                tableDeletionSteps.Add(string.Format("Delete table command sent to Amazon for table {0}, status after deletion: {1}", tableName
                    , tableDescription.TableStatus));
            }
            catch (AmazonDynamoDBException exception)
            {
                tableDeletionSteps.Add(string.Concat("Exception while deleting DynamoDb table: {0}", exception.Message));
                tableDeletionSteps.Add(string.Concat("Error code: {0}, error type: {1}", exception.ErrorCode, exception.ErrorType));
            }
            return tableDeletionSteps;
        }

        public async Task<List<string>> UpdateTableAsync(string tableName)
        {
            List<string> tableUpdateSteps = new List<string>();
            try
            {
                UpdateTableRequest updateTableRequest = new UpdateTableRequest();
                updateTableRequest.TableName = tableName;
                updateTableRequest.ProvisionedThroughput = new ProvisionedThroughput() { ReadCapacityUnits = 2, WriteCapacityUnits = 2 };
                UpdateTableResponse updateTableResponse = await _client.UpdateTableAsync(updateTableRequest);
                TableDescription tableDescription = updateTableResponse.TableDescription;
                tableUpdateSteps.Add(string.Format("Update table command sent to Amazon for table {0}, status after update: {1}", tableName
                    , tableDescription.TableStatus));
            }
            catch (AmazonDynamoDBException exception)
            {
                tableUpdateSteps.Add(string.Concat("Exception while updating DynamoDb table: {0}", exception.Message));
                tableUpdateSteps.Add(string.Concat("Error code: {0}, error type: {1}", exception.ErrorCode, exception.ErrorType));
            }
            return tableUpdateSteps;
        }

        public async Task<bool> InsertRecordAsync<T>(T value)
        {
            bool recordInserted = true;
            try
            {
                {
                    DynamoDBContext context = new DynamoDBContext(_client, _ContextConfig);
                    await context.SaveAsync<T>(value);
                }
            }
            catch (AmazonDynamoDBException exception)
            {
                recordInserted = false;

                Console.WriteLine(string.Concat("Exception while inserting records into DynamoDb table: {0}", exception.Message));
                Console.WriteLine(string.Concat("Error code: {0}, error type: {1}", exception.ErrorCode, exception.ErrorType));
            }
            return recordInserted;
        }

        public async Task<bool> DeleteRecordAsync<T>(string tableName, T value)
        {
            bool recordDeleted = true;
            try
            {
                DynamoDBContext context = new DynamoDBContext(_client);
                await context.DeleteAsync<T>(value);
            }
            catch (AmazonDynamoDBException exception)
            {
                recordDeleted = false;

                Console.WriteLine(string.Concat("Exception while inserting records into DynamoDb table: {0}", exception.Message));
                Console.WriteLine(string.Concat("Error code: {0}, error type: {1}", exception.ErrorCode, exception.ErrorType));
            }
            return recordDeleted;
        }

        public Task<T> SearchRecord<T>(string tableName, string primaryKey, string secondaryKey)
        {
            Task<T> record = null;
            try
            {
                DynamoDBContext context = new DynamoDBContext(_client);
                //get by id
                record = context.LoadAsync<T>(primaryKey, secondaryKey);
            }
            catch (AmazonDynamoDBException exception)
            {
                Console.WriteLine(string.Concat("Exception while filtering records in DynamoDb table: {0}", exception.Message));
                Console.WriteLine(string.Concat("Error code: {0}, error type: {1}", exception.ErrorCode, exception.ErrorType));
            }
            return record;
        }

        //public async Task<List<string>> ViewDocument(Document updatedDocument)
        //{
        //    foreach (var attribute in updatedDocument.GetAttributeNames())
        //    {
        //        string stringValue = null;
        //        var value = updatedDocument[attribute];

        //        if (value is Primitive)
        //            stringValue = value.AsPrimitive().Value.ToString();
        //        else if (value is PrimitiveList)
        //            stringValue = string.Join(",", (from primitive
        //                            in value.AsPrimitiveList().Entries
        //                                            select primitive.Value).ToArray());

        //        Console.WriteLine("{0} - {1}", attribute, stringValue);
        //    }
        //}
    }
}