﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Custo.Backend.Restaurant.DataAccess.DynamoDBHelper
{
    interface IDynamoDB
    {
        Task<List<string>> GetTablesList();

        Task<List<string>> GetTableDetails(string table);

        Task<List<string>> GetTablesDetails();

        Task<List<string>> CreateTableAsync(string tableName, string primaryKey, string secondaryKey);

        Task<List<string>> DeleteTableAsync(string tableName);

        Task<List<string>> UpdateTableAsync(string tableName);

        Task<bool> InsertRecordAsync<T>(T value);
    }
}
