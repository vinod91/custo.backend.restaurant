﻿using AutoMapper;
using DE = Custo.Backend.Restaurant.Entities.DB;
using BE = Custo.Backend.Restaurant.Entities.Business;

namespace Custo.Backend.Restaurant
{
    public class ProfileMapper:Profile
    {
        public ProfileMapper()
        {
            CreateMap<DE.Restaurant, BE.Restaurant>();
        }
    }
}
