﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Custo.Backend.Restaurant.Business;
using BE = Custo.Backend.Restaurant.Entities.Business;
using Custo.Backend.Restaurant.Entities.DB;

namespace Custo.Backend.Restaurant.Controllers
{
    [Route("api/[controller]")]
    public class RestaurantController : Controller
    {
        private readonly IRestaurantBusiness _restaurantBusiness;

        public RestaurantController(IRestaurantBusiness restaurantBusiness)
        {
            _restaurantBusiness = restaurantBusiness;
        }

        [HttpGet]
        [Route("TestPing")]
        public string TestPing()
        {
            return "Test";
        }

        [HttpGet]
        [Route("GetRestaurants/{latitude}/{longitude}")]
        public List<BE.Restaurant> GetRestaurants(float latitude, float longitude)
        {
            return _restaurantBusiness.GetResturantsBasedOnCoordinate(latitude, longitude);
        }

        [HttpGet]
        [Route("GetRestaurants/{searchParameter}")]
        public List<BE.Restaurant> GetRestaurants(string searchParameter)
        {
            return _restaurantBusiness.GetResturantsBasedOnNameOrFood(searchParameter);
        }

        [HttpGet]
        [Route("FetchRestaurantDetails/{restaurantId}")]
        public Task<RestaurantDetails> FetchRestaurantDetails(int restaurantId)
        {
            return  _restaurantBusiness.FetchRestaurantDetails(restaurantId);
        }
    }
}
