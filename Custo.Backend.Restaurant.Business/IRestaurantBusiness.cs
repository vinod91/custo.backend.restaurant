﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Custo.Backend.Restaurant.Entities.DB;
using BE = Custo.Backend.Restaurant.Entities.Business;

namespace Custo.Backend.Restaurant.Business
{
    public interface IRestaurantBusiness
    {
        List<BE.Restaurant> GetResturantsBasedOnCoordinate(float latitude, float longitude);
        List<BE.Restaurant> GetResturantsBasedOnNameOrFood(string searchParam);
        Task<RestaurantDetails> FetchRestaurantDetails(int restaurantId);
    }
}
