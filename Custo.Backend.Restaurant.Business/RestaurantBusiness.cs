﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Custo.Backend.Restaurant.DataAccess;
using Custo.Backend.Restaurant.Entities.DB;
using BE = Custo.Backend.Restaurant.Entities.Business;
namespace Custo.Backend.Restaurant.Business
{
    public class RestaurntBusiness : IRestaurantBusiness
    {
        private readonly IRestaurantRepository _restaurantRepository;
        private readonly IMapper _mapper;

        public RestaurntBusiness(IRestaurantRepository restaurantRepository, IMapper mapper)
        {
            _restaurantRepository = restaurantRepository;
            _mapper = mapper;
        }
        public List<BE.Restaurant> GetResturantsBasedOnCoordinate(float latitude, float longitude)
        {
            if (ValidateCoordinates(latitude, longitude))
            {
                return _mapper.Map<List<BE.Restaurant>>(_restaurantRepository.GetResturants(latitude, longitude));
            }
            return null;
        }

        public List<BE.Restaurant> GetResturantsBasedOnNameOrFood(string searchParams)
        {
            return _mapper.Map<List<BE.Restaurant>>(_restaurantRepository.GetResturants(searchParams));
        }

        public Task<RestaurantDetails> FetchRestaurantDetails(int restaurantId)
        {
          return  _restaurantRepository.FetchRestaurantDetails(restaurantId);
        }

        private bool ValidateCoordinates(float latitude, float longitude)
        {
            if (latitude > -90 && latitude < 90 & longitude > -180 & longitude < 180)
            {
                return true;
            }
            else
                return false;
        }
    }
}
