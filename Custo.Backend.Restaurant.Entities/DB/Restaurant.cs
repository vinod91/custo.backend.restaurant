﻿namespace Custo.Backend.Restaurant.Entities.DB
{
    public class Restaurant
    {
        public int RestaurantId { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }
        public string ImageUrl { get; set; }
        public double? Rating { get; set; }
        public int? Offer { get; set; }
        public int Cost { get; set; }
        public string ClosingTime { get; set; }
        public int DeliveryTime { get; set; }
        public string Cuisine { get; set; }
        public string SignatureDish { get; set; }
        public int CatalogueId { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Address { get; set; }
    }
}
