﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Custo.Backend.Restaurant.Entities.DB
{
    public class RestaurantDetails
    {
            public int cost { get; set; }
            public string description { get; set; }
            public dynamic food { get; set; }
            public string imageurl { get; set; }
            public string name { get; set; }
            public List<string> ordermode { get; set; }
            public int rating { get; set; }
            public int RestaurantId { get; set; }
            public string signaturedish { get; set; }
    }
}
