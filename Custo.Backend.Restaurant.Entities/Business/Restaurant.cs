﻿namespace Custo.Backend.Restaurant.Entities.Business
{
    public class Restaurant
    {
        public long RestaurantId { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }
        public string ImageUrl { get; set; }
        public float Rating { get; set; }
        public int Offer { get; set; }
        public int Cost { get; set; }
        public string ClosingTime { get; set; }
        public int DeliveryTime { get; set; }
        public string Cuisine { get; set; }
        public string SignatureDish { get; set; }
        public int CatalogueId { get; set; }
        public string Address { get; set; }
    }
}
